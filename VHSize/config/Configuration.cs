﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VHSize.config {
    public class Configuration {
        public ProcessStep Input { get; } = new ProcessStep();
        public ProcessStep Output { get; } = new ProcessStep();
    }

    public class ProcessStep { 
        public string Method { get; set; }
        //public Dictionary<string, object> Format { get; set; } = new Dictionary<string, object>();
        public ProcessFormat Format { get; } = new ProcessFormat();
    }

    public class ProcessFormat {
        public string Standard { get; set; } = "ntsc";
        public int Lines { get; set; } = 480;
    }
}
