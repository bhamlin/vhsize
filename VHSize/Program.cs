﻿using Newtonsoft.Json;
using System;
using System.IO;
using VHSize.config;

namespace VHSize {
    class Program {
        static void Main(string[] args) {
            Console.WriteLine("Hello World!");

            Configuration conf = JsonConvert.DeserializeObject<Configuration>(File.ReadAllText("conf\\ntsc.json"));
            Console.WriteLine(conf);
            Console.WriteLine(args[0]);

            if (System.Diagnostics.Debugger.IsAttached) {
                Console.ReadKey();
            }
        }
    }
}
